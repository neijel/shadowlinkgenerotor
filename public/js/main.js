function LinkGenerator() {
  this.url = false;
  this.datetime = true;
}

LinkGenerator.prototype = {

  buttonEvent: function(url) {

    if (this.url && this.datetime) {
      let _this = this;

      $.ajax({
        type: 'POST',
        url: '/api/v1/genlink',
        dataType: 'JSON',
        data: url.serialize(),
        error: function(xhr, b, c) {
          console.log("xhr=" + xhr + " b=" + b + " c=" + c);
        }
      }).done(function(data) {

        if (data === 'false') {
          return $('#error').html('Allready used!')
        }

        return _this.setResult(data);

      }).fail(function() {

        throw "AJAX if failur";
      })

    } else if (!this.datetime) {
      $('#error').html('Date less than now date!')
    }
    return this;
  },

  setResult: function(data) {
    $('.genFrame').css('display', 'none')
    let content = "<a href = '" + window.location.href + data.path + "'>Your Link! ID: " + data.path + " </a><p>" + window.location.href + data.path + "</p>" +
      ((data.lifetime != undefined) ? "<p>Link expires: " + data.lifetime.split('T')[0] + " " + data.lifetime.split('T')[1] + "</p>" : '');
    $('.userRes').css('display', 'block').find('#res').html(content)
  },

  checkDataTime: function() {
    let date = $('#datapicker').val(),
      time = $('#timepicker').val();

    if (date != '' && time == '') $('#timepicker').val("23:59")
    if (time != '' && date == '') $('#datapicker').val(new Date().toISOString().split('T')[0])

    return this;
  },

  validUrl: function() {

    let url = $('#geturl').val(),
      validator = new validURL(url);

    if (validator.is_URL()) {

      this.url = true;
      $('#error').html('')
    } else {


      if (!url) $('#error').html('Please fill URL.')
      else $('#error').html('Incorect URL.')
    }

    return this;
  },

  prepare: function() {
    this.validUrl().checkDataTime();

    return this;
  }

}

linkg = new LinkGenerator;



$('#getLink').on('click', function(e) {
  e.preventDefault();
  linkg.prepare().buttonEvent($(".genFrame"));
});

$('#switchSetting').on('click', () => {
  let val = ($('#switcher').is(':checked')) ? 'flex' : 'none';
  $('#setting').css({
    'display': val
  })

})

$('#datapicker').on('blur', function() {
  let val = $(this).val(),
    date = new Date().toISOString().split('T')[0];

  if (val == '' || val < date) {
    $(this).css('border-color', "rgba(239, 96, 96, 0.73)");
  } else {
    $(this).css('border-color', "rgba(66, 210, 61, 0.69)");
  }
  linkg.datetime = (val != '' && val < date) ? false : true;

})


$('#timepicker').on('blur', function() {
  if ($(this).val() == '') {
    $(this).css('border-color', "rgba(239, 96, 96, 0.73)");
  } else {
    $(this).css('border-color', "rgba(66, 210, 61, 0.69)");
  }
})

$('#passCheck').on('click', function(e) {
  e.preventDefault();

  console.log($('.genFrame').serialize());
  if ($('#passreq').val() != '') {

    var timezone = jstz.determine(),
      reciveData = $('.genFrame').serialize();

    $.ajax({
      type: 'POST',
      url: '/api/v1/checklink',
      dataType: 'JSON',
      data: reciveData,
      error: function(xhr, b, c) {
        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
      }
    }).done(function(data) {
      console.log(data);
      if (data != 'false') {
        window.location = data;
      } else {
        $('#error').html('Incrorect Pass! ')
      }
    })
  } else {
    $('#error').html('Please fill area!')
  }

})
