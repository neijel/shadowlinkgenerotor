function validURL(value) {
  this.value = value;
}

validURL.prototype = {
  splitUri: function() {
    var splitted = this.value.match(/(?:([^:\/?#]+):)?(?:\/\/([^\/?#]*))?([^?#]*)(?:\?([^#]*))?(?:#(.*))?/);

    return splitted;
  },

  is_URL: function() {
    if (!this.value) {
      return;
    }
    // check for illegal characters
    if (/[^a-z0-9\:\/\?\#\[\]\@\!\$\&\'\(\)\*\+\,\;\=\.\-\_\~\%]/i.test(this.value)) return;

    // check for hex escapes that aren't complete
    if (/%[^0-9a-f]/i.test(this.value)) return;
    if (/%[0-9a-f](:?[^0-9a-f]|$)/i.test(this.value)) return;

    var splitted = [];
    var scheme = '';
    var authority = '';
    var path = '';
    var query = '';
    var fragment = '';
    var out = '';

    // from RFC 3986
    splitted = this.splitUri();
    scheme = splitted[1];
    authority = splitted[2];
    path = splitted[3];
    query = splitted[4];
    fragment = splitted[5];

    // scheme and path are required, though the path can be empty
    if (!(scheme && scheme.length) && path.length == 0) return;

    // if authority is present, the path must be empty or begin with a /
    // console.log(!/w{0,3}\.?[\w]+\.[\w]+\/?[\w]?$/.test(path));
    if (authority && authority.length) {
      if (!(path.length === 0 || /^\//.test(path))) return;
    } else {
      // if authority is not present, the path must not start with //
      if (/^\/\//.test(path) || !/w{0,3}\.?[\w]+\.[\w]+\/?[\w]?$/.test(path)) return;
    }

    let testURL = scheme == undefined ? path : scheme;

    // scheme must begin with a letter, then consist of letters, digits, +, ., or -
    if (!/^[a-z][a-z0-9\+\-\.\/]*$/.test(testURL.toLowerCase())) return;

    // re-assemble the URL per section 5.3 in RFC 3986
    out += testURL + ':';
    if (authority && authority.length) {
      out += '//' + authority;
    }

    if (scheme != undefined)
      out += path;

    if (query && query.length) {
      out += '?' + query;
    }

    if (fragment && fragment.length) {
      out += '#' + fragment;
    }

    return out;
  }
}
