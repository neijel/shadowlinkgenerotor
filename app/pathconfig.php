<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pathconfig extends Model
{
    protected $fillable = ['id_url','path','pass','lifetime'];
}
