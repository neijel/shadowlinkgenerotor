<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class urlstore extends Model
{
    protected $fillable = ['url','path','pass','lifetime'];
}
