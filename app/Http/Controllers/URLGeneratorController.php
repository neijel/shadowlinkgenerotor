<?php

namespace App\Http\Controllers;

use App\urlstore as UStore;
use App\pathconfig;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class URLGeneratorController extends Controller
{
    public function createLink(Request $request)
    {
        $patter = "/((https?:\/\/)?([\w\.]+))\/?(.+)/";

        preg_match("/^(https?:\/\/)/", $request['url'], $matches);

        preg_match($patter, $request['url'], $res);



        $data['url'] =  $res[0];

        if (count($matches) == 0) {
            $data['url'] = 'http://' .  $data['url'];
        }

        $data['path'] = self::khash($res[0]);

        $checkLink = self::getRecod($data['path']);

        if ($checkLink) {
            return  response()->json('false');
        }

        self::initDate($request, $data);
        self::initPass($request, $data);

        return response()->json(self::setRecod($data));
        // return response()->json();
    }

    private static function khash($data)
    {
        static $map = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $hash = bcadd(sprintf('%u', crc32($data)), 0x100000000);
        $str = "";
        do {
            $str = $map[bcmod($hash, 62) ] . $str;
            $hash = bcdiv($hash, 62);
        } while ($hash >= 1);
        return $str;
    }


    private static function getRecod($hash)
    {
        if (class_exists('Memcache') and Cache::has($hash)) {
            return Cache::get($hash);
        }
        return UStore::where('path', $hash)->first();
    }

    private static function setRecod($data)
    {
        if (Cache::has($data['path'])) {
            return 'false';
        }
        if (class_exists('Memcache')) {
            Cache::set($data['path'], $data);
        }

        UStore::create($data);
        return $data;
    }

    private static function dellRecord($id)
    {
        $record = UStore::find($id);
        $record->delete();
    }

    private static function initDate(Request $request, &$data)
    {
        $date = $request['date'];
        $time = $request['time'];

        if ($date || $time) {
            $data['lifetime'] = $date . (($date != '' && $time != '')?'T':'') . $time;
        }
    }

    private static function initPass(Request $request, &$data)
    {
        //if need encode use it function
        if ($request['pass']) {
            $data['pass'] = $request['pass'];
        }
    }

    public function redirectMe(Request $request, $hash)
    {
        $getRecord = self::getRecod($hash);

        if (count($getRecord) > 0) {
            $lifetime = strtotime($getRecord->lifetime);

            if ($lifetime > time() || $lifetime == '') {
                if ($getRecord->pass) {
                    return redirect('?hash='.$hash);
                }

                if ($getRecord->url) {
                    return redirect($getRecord->url);
                }
            } else {
                self::dellRecord($getRecord->id);
            }
        }

        return response(view("error.404"), 404);
    }

    public function passValidator(Request $request)
    {
        $pass = $request['pass'];
        $hash = $request['hash'];

        $getVAl = self::getRecod($hash);

        if ($pass != $getVAl->pass) {
            return response()->json('false');
        } else {
            return response()->json($getVAl->url);
        }
    }
}
// http://192.168.0.238/8EBIR5
