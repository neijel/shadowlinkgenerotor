<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\UrlGenerator as URL;

class MainPageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public static $id;

    public function index()
    {
        return $this->concate('site.main');
    }

    public function passRequset($id)
    {
        self::$id = $id;
        return $this->concate('site.passRequest');
    }

    private function concate($page)
    {
        return response(view('templates.header') . view($page) . view('templates.footer') . view('templates.conjs'));
    }

    public function execHashURL($id)
    {
        return $id;
    }
}
