<footer class="footer">
  <div class="container">
    <div class="text-muted">
        <span class="pull-left">ShadowLink <sup>&copy;</sup> Netrix Group</span>
        <span class="pull-right">v 0.0.1</span>
    </div>
  </div>
</footer>



<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=PT+Sans&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/min/siteStyle-min.css') }}" >
