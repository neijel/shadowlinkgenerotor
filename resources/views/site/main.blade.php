<?php
$hash = app('request')->input('hash');
?>

  <section class="main">
    <div class="content">

      <form class="genFrame">
        @if ($hash == '')
        <p id='hello'> Wellcome to ShadowLinkGenerator </p>
        <p> Enter your link </p>
        <p id='error'> </p>
        <input type="text" id='geturl' name='url'>
        <div class='AdvSetting'>
          <label id='switchSetting'>
          <input type="checkbox"  id = 'switcher'>
          <span> Activate Addition Settings</span>
        </label>
          <div id="setting">
            <label>Input Password
          <input type="password" name='pass'></label>
            <label>Choose lifetime
          <input type="date" name = 'date' id = 'datapicker' message= "Incorect Date!">  </label>
            <input type="time" name='time' id='timepicker'>
            <!-- <div id='vline'></div>
            <div>
              <p> Or lifetime in [hours:min] </p>
              <input type="time" name='timeSimple' id='timepicker2'>
            </div> -->
          </div>
        </div>
        <button id="getLink" class='btn btn-success'>Gen!</button>
      </form>
      <div class="userRes">
        <div id="res"></div>
        <button onclick="location.reload();" class="btn btn-info">Generate Again</button>
      </div>
      @else
      <div class='PassRequest'>
        <input type="hidden" value="{{ $hash  }}" name='hash'>
        <p> Enter Your Pass </p>
        <p id='error'> </p>

        <input type="password" name="pass" id='passreq'>

        <button id="passCheck" class='btn btn-success'>GO!</button>
      </div>
    </div>
    @endif
  </section>
