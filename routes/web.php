<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', 'MainPageController@index');
$app->get('/{hash}', 'URLGeneratorController@redirectMe');
// $app->get('/ReguestPass/{id}', 'MainPageController@passRequset');

$app->group(['prefix'=>'api/v1'], function () use ($app) {
    $class = 'URLGeneratorController';

    $app->post('genlink', $class.'@createLink');
    $app->post('checklink', $class.'@passValidator');
});
